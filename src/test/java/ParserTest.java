import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class ParserTest {
    @Test
    public void should_read_in_all_telemetry_data_from_file() {
        final int totalTelemetryRecords = 14;

        ArrayList<TelemetryData> data = Parser.readAndParseFile("src/test/resources/sample.txt");

        assertEquals(totalTelemetryRecords, data.size());
    }

    @Test
    public void should_parse_dates_from_telemetry_data() throws ParseException {
        ArrayList<TelemetryData> data = Parser.readAndParseFile("src/test/resources/sample.txt");

        assertEquals(data.get(0).getDate(), new SimpleDateFormat("MM-dd-yyyy HH:mm:ss.SSS")
                .parse("01-01-2018 23:01:05.001"));

        assertEquals(data.get(5).getDate(), new SimpleDateFormat("MM-dd-yyyy HH:mm:ss.SSS")
                .parse("01-01-2018 23:02:09.014"));
    }

    @Test
    public void should_parse_satellite_id_from_telemetry_data() {
        ArrayList<TelemetryData> data = Parser.readAndParseFile("src/test/resources/sample.txt");

        assertEquals(data.get(1).getSatelliteId(), 1000.0);
        assertEquals(data.get(12).getSatelliteId(), 1001.0);
    }

    @Test
    public void should_parse_component_from_telemetry_data() {
        ArrayList<TelemetryData> data = Parser.readAndParseFile("src/test/resources/sample.txt");

        assertEquals(data.get(2).getComponentType(), "TSTAT");
        assertEquals(data.get(7).getComponentType(), "BATT");
    }
}