import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class AlertsTest {
    @Test
    void should_generate_one_thermostat_alert_and_one_battery_alert_from_telemetry_data() {
        /**
         * sample.txt has one thermostat alert and one battery alert.
         */
        ArrayList<TelemetryData> data = Parser
                .readAndParseFile("src/test/resources/sample.txt");

        Alerts alerts = new Alerts(data);

        alerts.generateAlerts();

        assertEquals(1, alerts.getThermostatAlerts().size());
        assertEquals(1, alerts.getBatteryAlerts().size());
    }

    @Test
    void should_generate_one_thermostat_alert_from_telemetry_data() {
        /**
         * sample.alerts_A.txt has only one thermostat alert
         */
        ArrayList<TelemetryData> data = Parser
                .readAndParseFile("src/test/resources/sample_alerts_A.txt");

        Alerts alerts = new Alerts(data);

        alerts.generateAlerts();

        assertEquals(1, alerts.getThermostatAlerts().size());
        assertEquals(0, alerts.getBatteryAlerts().size());
    }

    @Test
    void should_generate_one_battery_alert_from_telemetry_data() {
        /**
         * sample.alerts_B.txt has only one battery alert
         */
        ArrayList<TelemetryData> data = Parser
                .readAndParseFile("src/test/resources/sample_alerts_B.txt");

        Alerts alerts = new Alerts(data);

        alerts.generateAlerts();

        assertEquals(0, alerts.getThermostatAlerts().size());
        assertEquals(1, alerts.getBatteryAlerts().size());
    }
}