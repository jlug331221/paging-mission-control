import java.io.BufferedReader;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Class for parsing the telemetry data.
 * */
public class Parser {
    /**
     * Read telemetry data from fileName and parse information.
     *
     * @param fileName - file containing telemetry status data
     * @return - list of telemetry data
     */
    public static ArrayList<TelemetryData> readAndParseFile(String fileName) {
        try {
            ArrayList<TelemetryData> data = new ArrayList<>();

            FileReader inputFile = new FileReader(fileName);

            BufferedReader bufferedReader = new BufferedReader(inputFile);

            String line = "";

            while((line = bufferedReader.readLine()) != null) {
                String[] recordInfo = line.split("\\|");

                data.add(new TelemetryData(
                        parseDate(recordInfo[0]),
                        Integer.parseInt(recordInfo[1]),
                        Float.parseFloat(recordInfo[2]),
                        Float.parseFloat(recordInfo[3]),
                        Float.parseFloat(recordInfo[4]),
                        Float.parseFloat(recordInfo[5]),
                        Float.parseFloat(recordInfo[6]),
                        recordInfo[7]));
            }

            return data;
        } catch(Exception e) {
            System.out.println("ERROR: problem reading file...");
            System.out.println(e.getMessage());

            return null;
        }
    }

    /**
     * Parse the date in the following format:
     *   "yyyyMMdd HH:mm:ss.SSS"
     *
     * @param date - date to parse
     * @return - parsed date
     * @throws ParseException - throw exception if parsing fails
     */
    private static Date parseDate(String date) throws ParseException {
        return new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS").parse(date);
    }
}
