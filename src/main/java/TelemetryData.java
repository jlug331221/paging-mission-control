import java.util.Date;

public class TelemetryData {
    private final Date date;
    private final int satelliteId;
    private final float redHighLimit;
    private final float yellowHighLimit;
    private final float yellowLowLimit;
    private final float redLowLimit;
    private final float rawData;
    private final String componentType;

    public TelemetryData(Date date, int satelliteId, float redHighLimit, float yellowHighLimit,
                         float yellowLowLimit, float redLowLimit, float rawData,
                         String componentType) {
        this.date = date;
        this.satelliteId = satelliteId;
        this.redHighLimit = redHighLimit;
        this.yellowHighLimit = yellowHighLimit;
        this.yellowLowLimit = yellowLowLimit;
        this.redLowLimit = redLowLimit;
        this.rawData = rawData;
        this.componentType = componentType;
    }

    public Date getDate() {
        return date;
    }

    public int getSatelliteId() {
        return satelliteId;
    }

    public float getYellowHighLimit() {
        return yellowHighLimit;
    }

    public float getRedHighLimit() {
        return redHighLimit;
    }

    public float getYellowLowLimit() {
        return yellowLowLimit;
    }

    public float getRedLowLimit() {
        return redLowLimit;
    }

    public float getRawData() {
        return rawData;
    }

    public String getComponentType() {
        return componentType;
    }
}
