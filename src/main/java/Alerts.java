import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.*;

/**
 * Class for generating alerts and printing them to the console.
 */
public class Alerts {
    private HashMap<Integer, ArrayList<TelemetryData>> thermostatData;
    private HashMap<Integer, ArrayList<TelemetryData>> batteryData;

    private ArrayList<Integer> thermostatAlerts;
    private ArrayList<Integer> batteryAlerts;

    private static final int INTERVAL_LENGTH = 5;
    private static final int INDENT_FACTOR = 4;

    public Alerts(ArrayList<TelemetryData> data) {
        thermostatData = new HashMap<>();
        batteryData = new HashMap<>();

        thermostatAlerts = new ArrayList<>();
        batteryAlerts = new ArrayList<>();

        gatherTelemetryReadings(data);
    }

    /**
     * Gather telemetry status readings from telemetryData.
     *
     * @param telemetryData - telemetry data with status readings
     */
    private void gatherTelemetryReadings(ArrayList<TelemetryData> telemetryData) {
        int index = 0;
        for (TelemetryData telemetryReading: telemetryData) {
            if (telemetryData.get(index).getComponentType().equals("TSTAT")) {
                thermostatData.computeIfAbsent(
                        telemetryData.get(index).getSatelliteId(), k -> new ArrayList<>())
                            .add(telemetryData.get(index));
            }

            if (telemetryData.get(index).getComponentType().equals("BATT")) {
                batteryData.computeIfAbsent(
                                telemetryData.get(index).getSatelliteId(), k -> new ArrayList<>())
                        .add(telemetryData.get(index));
            }

            index++;
        }
    }

    /**
     * Print the alerts (temperature and battery) to the console with JSON formatting.
     */
    public void printToConsole() {
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        TelemetryData telemetryData = null;

        for (Integer satID: thermostatAlerts) {
            telemetryData = thermostatData.get(satID).get(0);

            jsonObject.put("satelliteId", telemetryData.getSatelliteId());
            jsonObject.put("severity", "RED HIGH");
            jsonObject.put("component", telemetryData.getComponentType());
            jsonObject.put("timestamp", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss:SSS'Z'")
                    .format(telemetryData.getDate()));

            jsonArray.put(jsonObject);

            jsonObject = null;
        }

        jsonObject = new JSONObject();

        for (Integer satID: batteryAlerts) {
            telemetryData = batteryData.get(satID).get(0);

            jsonObject.put("satelliteId", telemetryData.getSatelliteId());
            jsonObject.put("severity", "RED LOW");
            jsonObject.put("component", telemetryData.getComponentType());
            jsonObject.put("timestamp", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss:SSS'Z'")
                    .format(telemetryData.getDate()));

            jsonArray.put(jsonObject);

            jsonObject = null;
        }

        System.out.println(jsonArray.toString(INDENT_FACTOR));
    }

    /**
     * Generate alerts for thermostat and battery data.
     */
    public void generateAlerts() {
        generateThermostatAlerts();

        generateBatteryAlerts();
    }

    /**
     * Generate the thermostat alerts.
     */
    private void generateThermostatAlerts() {
        for (Map.Entry<Integer, ArrayList<TelemetryData>> entry: thermostatData.entrySet()) {
            if (generateThermostatAlert(entry.getValue())) {
                thermostatAlerts.add(entry.getKey());
            }
        }
    }

    /**
     * Determine whether to generate a thermostat alert from the thermostat telemetry data.
     *
     * @param telemetryThermostatData - thermostat data from the telemetry readings
     * @return - true if we generate a thermostat alert and false otherwise
     */
    private boolean generateThermostatAlert(ArrayList<TelemetryData> telemetryThermostatData) {
        ArrayList<Date> timestamps = new ArrayList<>();

        for (TelemetryData thermostatReading: telemetryThermostatData) {
            if (thermostatReading.getRawData() > thermostatReading.getRedHighLimit()) {
                timestamps.add(thermostatReading.getDate());
            }
        }

        if (timestamps.size() >= 3) { // only check if there are at least 3 timestamps
            return withinTimeInterval(timestamps);
        }

        return false;
    }

    /**
     * Generate the battery alerts.
     */
    private void generateBatteryAlerts() {
        for (Map.Entry<Integer, ArrayList<TelemetryData>> entry: batteryData.entrySet()) {
            if (generateBatteryAlert(entry.getValue())) {
                batteryAlerts.add(entry.getKey());
            }
        }
    }

    /**
     * Determine whether to generate a battery alert from the battery telemetry data.
     *
     * @param telemetryBatteryData - battery data from the telemetry readings
     * @return - true if we generate a battery alert and false otherwise
     */
    private boolean generateBatteryAlert(ArrayList<TelemetryData> telemetryBatteryData) {
        ArrayList<Date> timestamps = new ArrayList<>();

        for (TelemetryData batteryReading: telemetryBatteryData) {
            if (batteryReading.getRawData() < batteryReading.getRedLowLimit()) {
                timestamps.add(batteryReading.getDate());
            }
        }

        if (timestamps.size() >= 3) { // only check if there are at least 3 timestamps
            return withinTimeInterval(timestamps);
        }

        return false;
    }

    /**
     * Determine if the timestamps are within a specific time interval.
     *
     * @param timestamps - timestamps to check
     * @return - true if the timestamps are within an interval and false otherwise
     */
    private boolean withinTimeInterval(ArrayList<Date> timestamps) {
        // Do we assume timestamps are already sorted? If not, then we can sort here
        // timestamps.sort(Date::compareTo);

        Instant intervalBefore = null;
        Instant intervalAfter = null;
        Instant currentTimestamp = null;

        int timestampsWithinInterval = 0;

        for (Date timestamp: timestamps) {
            currentTimestamp = timestamp.toInstant();

            intervalBefore = currentTimestamp.minus(INTERVAL_LENGTH, ChronoUnit.MINUTES);
            intervalAfter = currentTimestamp.plus(INTERVAL_LENGTH, ChronoUnit.MINUTES);

            timestampsWithinInterval = withinTimeIntervalHelper(
                    timestamps, currentTimestamp, intervalBefore, intervalAfter);

            if (timestampsWithinInterval >= 3) { return true; }
        }

        return false;
    }

    /**
     * Helper for checking if there are timestamps within the range of intervalBefore and
     * intervalAfter.
     *
     * @param timestamps - timestamps to check
     * @param currentTimestamp - current timestamp
     * @param intervalBefore - interval before the current timestamp
     * @param intervalAfter - interval after the current timestamp
     * @return - number of timestamps that are between intervalBefore and intervalAfter
     */
    private int withinTimeIntervalHelper(ArrayList<Date> timestamps, Instant currentTimestamp,
                                         Instant intervalBefore, Instant intervalAfter) {
        Instant timestampToCheck = null;
        int timestampsWithinInterval = 0;

        for (Date timestamp: timestamps) {
            timestampToCheck = timestamp.toInstant();

            if (timestampToCheck == currentTimestamp ||
                    (!timestampToCheck.isBefore(intervalBefore)) &&
                            timestampToCheck.isBefore(intervalAfter))
            {
                timestampsWithinInterval++;
            }
        }

        return timestampsWithinInterval;
    }

    public HashMap<Integer, ArrayList<TelemetryData>> getThermostatData() {
        return thermostatData;
    }

    public void setThermostatData(HashMap<Integer, ArrayList<TelemetryData>> thermostatData) {
        this.thermostatData = thermostatData;
    }

    public HashMap<Integer, ArrayList<TelemetryData>> getBatteryData() {
        return batteryData;
    }

    public void setBatteryData(HashMap<Integer, ArrayList<TelemetryData>> batteryData) {
        this.batteryData = batteryData;
    }

    public ArrayList<Integer> getThermostatAlerts() {
        return thermostatAlerts;
    }

    public void setThermostatAlerts(ArrayList<Integer> thermostatAlerts) {
        this.thermostatAlerts = thermostatAlerts;
    }

    public ArrayList<Integer> getBatteryAlerts() {
        return batteryAlerts;
    }

    public void setBatteryAlerts(ArrayList<Integer> batteryAlerts) {
        this.batteryAlerts = batteryAlerts;
    }
}
