import java.util.ArrayList;

public class App {
    public static void main(String[] args) {
        if (args.length != 1) {
            throw new IllegalArgumentException("ERROR: incorrect program arguments");
        }

        ArrayList<TelemetryData> telemetryData = Parser.readAndParseFile(args[0]);

        Alerts alerts = new Alerts(telemetryData);

        alerts.generateAlerts();

        alerts.printToConsole();
    }
}
